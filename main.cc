#include <iomanip>
#include <numeric>
#include <thread>

#include <iostream>
#include <omp.h>
#include <unistd.h>

#include "common.h"
#include "config.h"
#if defined(REPORT_FREQUENCY)
#include <fmt/fmt.h>
#endif
#if defined(REPORT_ENERGY)
#include "pmt.h"
#endif

template <class T> T *allocate_memory(size_t n) {
  void *ptr = nullptr;
  if (n > 0) {
    size_t bytes = n * sizeof(T);
    bytes = (((bytes - 1) / ALIGNMENT) * ALIGNMENT) + ALIGNMENT;
    if (posix_memalign(&ptr, ALIGNMENT, bytes) != 0) {
      std::cerr << "Could not allocate " << bytes << " bytes" << std::endl;
      exit(EXIT_FAILURE);
    };
  }
  return (T *)ptr;
}

void init(float *ptr, size_t n) {
#pragma omp parallel for
  for (size_t i = 0; i < n; i++) {
    ptr[i] = 1;
  }
}

int main() {
  int nr_threads = 0;
#pragma omp parallel
  { nr_threads = omp_get_num_threads(); }

  float *a_xx_real = allocate_memory<float>(R);
  float *a_xy_real = allocate_memory<float>(R);
  float *a_yx_real = allocate_memory<float>(R);
  float *a_yy_real = allocate_memory<float>(R);
  float *a_xx_imag = allocate_memory<float>(R);
  float *a_xy_imag = allocate_memory<float>(R);
  float *a_yx_imag = allocate_memory<float>(R);
  float *a_yy_imag = allocate_memory<float>(R);
  float *b_real = allocate_memory<float>(R);
  float *b_imag = allocate_memory<float>(R);
  float *c_real = allocate_memory<float>(nr_threads * 4);
  float *c_imag = allocate_memory<float>(nr_threads * 4);

#if defined(REPORT_ENERGY)
  std::unique_ptr<pmt::PMT> sensor = pmt::rapl::Rapl::create();
#endif

  // Find value of N for which the benchmark takes at least
  // runtime_min seconds. This process also serves as a warmup.
  int N = 1024;
  double runtime = 0;
  double runtime_min = 1; // seconds
  while (true) {
    runtime = -omp_get_wtime();
#pragma omp parallel
    {
      nr_threads = omp_get_num_threads();
      kernel(N, a_xx_real, a_xy_real, a_yx_real, a_yy_real, a_xx_imag,
             a_xy_imag, a_yx_imag, a_yy_imag, b_real, b_imag, c_real, c_imag);
    }
    runtime += omp_get_wtime();
    if (runtime >= runtime_min) {
      break;
    } else {
      N *= 2;
    }
  }

  // Peform the benchmark
  for (int b = 0; b < B; b++) {
#if defined(REPORT_FREQUENCY)
    fmt::cpu::CPU cpu;
    std::vector<int> frequencies;
#endif
#if defined(REPORT_ENERGY)
    pmt::State start = sensor->read();
#endif
    runtime = -omp_get_wtime();
#pragma omp sections
    {
#pragma omp section
      {
#pragma omp parallel
          {nr_threads = omp_get_num_threads();
      kernel(N, a_xx_real, a_xy_real, a_yx_real, a_yy_real, a_xx_imag,
             a_xy_imag, a_yx_imag, a_yy_imag, b_real, b_imag, c_real, c_imag);
    }
  }
#pragma omp section
  {
#if defined(REPORT_FREQUENCY)
    usleep(1000);
    frequencies = cpu.get();
#endif
  }
}
runtime += omp_get_wtime();
auto flops = (R * N * 2 * 8 * 2) + 8;
auto gflops = nr_threads * flops * 1e-9;
auto bytes = (R * N * 10 * sizeof(float)) + 8;
auto gbytes = nr_threads * bytes * 1e-9;
std::cout << std::fixed << std::setprecision(2);
std::cout << runtime << " s, ";
std::cout << gflops / runtime << " GFlop/s, ";
std::cout << gbytes / runtime << " GByte/s";
#if defined(REPORT_FREQUENCY)
std::cout << ", ";
const int frequency =
    std::accumulate(frequencies.begin(), frequencies.end(), 0) /
    frequencies.size();
std::cout << frequency << " Mhz";
#endif
#if defined(REPORT_ENERGY)
pmt::State end = sensor->read();
std::cout << ", ";
double watts = pmt::PMT::watts(start, end);
std::cout << watts << " W, ";
std::cout << gflops / watts << " GFLOP/W";
#endif
std::cout << std::endl;
}

free(a_xx_real);
free(a_xy_real);
free(a_yx_real);
free(a_yy_real);
free(a_xx_imag);
free(a_xy_imag);
free(a_yx_imag);
free(a_yy_imag);
free(b_real);
free(b_imag);
free(c_real);
free(c_imag);

return EXIT_SUCCESS;
}
