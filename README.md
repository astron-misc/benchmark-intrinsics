# Benchmark intrinsics
This benchmark measures the performance of a simplified version of the
inner-most loop of the Image-Domain Gridding (IDG) gridder kernel,
corresponding to:
```pixel[4] += visibilities[n][4] * phasor[n]```

All variables are of type `complex<float>`. Thus the computation comprises
`n` complex multiplications and additions (of `visibilities` and `phasor`), followed by
a reduction to get the `pixel` value.

Three sets of benchmarks are available:
- Plain C code, up to the compiler to optimize.
- Hand vectorized with Intel intrinsics (for x86_64)
- Hand vectorized with IBM intrinsics (for ppc64le)

The x86_64 benchmarks are built for "native" (using the instruction set of the
build host), SSE (Westmere), AVX (Sandy Bridge), AVX+FMA (Haswell) and AVX-512 (Skylake).

The ppc64le benchmarks are built for AltiVec and AltiVec targetting Power 8 and Power 9.
