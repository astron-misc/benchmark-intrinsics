#include <immintrin.h>

#include "common.h"

#define VECTOR_LENGTH 4

// https://bit.ly/2erPT8t
inline float _mm_horizontal_add(__m128 x) {
  /* x0, x1, x2, x3 */
  __m128 x1 = x;

  /* x1, x1, x3, x3 */
  __m128 x2 = _mm_movehdup_ps(x1);

  /* x0+x1, -, x2+x3, - */
  __m128 x3 = _mm_add_ps(x1, x2);

  /* x2+x3, -, -, - */
  __m128 x4 = _mm_movehl_ps(x2, x3);

  /* x0+x1+x2+x3, -, -, - */
  __m128 x5 = _mm_add_ss(x3, x4);

  return _mm_cvtss_f32(x5);
}

void kernel(const unsigned long long n, const float *a_xx_real,
            const float *a_xy_real, const float *a_yx_real,
            const float *a_yy_real, const float *a_xx_imag,
            const float *a_xy_imag, const float *a_yx_imag,
            const float *a_yy_imag, const float *b_real, const float *b_imag,
            float *__restrict__ c_real, float *__restrict__ c_imag) {
#if USE_ALIGNED
  ASSUME_ALIGNED
#endif

  float sums[8] = {0.0f};

  for (unsigned int r = 0; r < R; r++) {
    __m128 c_xx_r = _mm_setzero_ps();
    __m128 c_xy_r = _mm_setzero_ps();
    __m128 c_yx_r = _mm_setzero_ps();
    __m128 c_yy_r = _mm_setzero_ps();
    __m128 c_xx_i = _mm_setzero_ps();
    __m128 c_xy_i = _mm_setzero_ps();
    __m128 c_yx_i = _mm_setzero_ps();
    __m128 c_yy_i = _mm_setzero_ps();

    for (unsigned int i = 0; i < n; i += VECTOR_LENGTH) {
      __m128 a_xx, a_xy, a_yx, a_yy;
      __m128 b_r, b_i;

      b_r = _mm_load_ps(&b_real[i]);
      b_i = _mm_load_ps(&b_imag[i]);

      // Load real part of input
      a_xx = _mm_load_ps(&a_xx_real[i]);
      a_xy = _mm_load_ps(&a_xy_real[i]);
      a_yx = _mm_load_ps(&a_yx_real[i]);
      a_yy = _mm_load_ps(&a_yy_real[i]);

      // Update output
      c_xx_r = _mm_add_ps(c_xx_r, _mm_mul_ps(a_xx, b_r));
      c_xx_i = _mm_add_ps(c_xx_i, _mm_mul_ps(a_xx, b_i));
      c_xy_r = _mm_add_ps(c_xy_r, _mm_mul_ps(a_xy, b_r));
      c_xy_i = _mm_add_ps(c_xy_i, _mm_mul_ps(a_xy, b_i));
      c_yx_r = _mm_add_ps(c_yx_r, _mm_mul_ps(a_yx, b_r));
      c_yx_i = _mm_add_ps(c_yx_i, _mm_mul_ps(a_yx, b_i));
      c_yy_r = _mm_add_ps(c_yy_r, _mm_mul_ps(a_yy, b_r));
      c_yy_i = _mm_add_ps(c_yy_i, _mm_mul_ps(a_yy, b_i));

      // Load imag part of input
      a_xx = _mm_load_ps(&a_xx_imag[i]);
      a_xy = _mm_load_ps(&a_xy_imag[i]);
      a_yx = _mm_load_ps(&a_yx_imag[i]);
      a_yy = _mm_load_ps(&a_yy_imag[i]);

      // Update output
      c_xx_r = _mm_sub_ps(c_xx_r, _mm_mul_ps(a_xx, b_i));
      c_xx_i = _mm_add_ps(c_xx_i, _mm_mul_ps(a_xx, b_r));
      c_xy_r = _mm_sub_ps(c_xy_r, _mm_mul_ps(a_xy, b_i));
      c_xy_i = _mm_add_ps(c_xy_i, _mm_mul_ps(a_xy, b_r));
      c_yx_r = _mm_sub_ps(c_yx_r, _mm_mul_ps(a_yx, b_i));
      c_yx_i = _mm_add_ps(c_yx_i, _mm_mul_ps(a_yx, b_r));
      c_yy_r = _mm_sub_ps(c_yy_r, _mm_mul_ps(a_yy, b_i));
      c_yy_i = _mm_add_ps(c_yy_i, _mm_mul_ps(a_yy, b_r));
    }

    // Reduce all vectors
    sums[0] += _mm_horizontal_add(c_xx_r);
    sums[1] += _mm_horizontal_add(c_xy_r);
    sums[2] += _mm_horizontal_add(c_yx_r);
    sums[3] += _mm_horizontal_add(c_yy_r);
    sums[4] += _mm_horizontal_add(c_xx_i);
    sums[5] += _mm_horizontal_add(c_xy_i);
    sums[6] += _mm_horizontal_add(c_yx_i);
    sums[7] += _mm_horizontal_add(c_yy_i);
  }

  // Update output
  unsigned int offset = 4 * omp_get_thread_num();
  c_real[offset + 0] = sums[0];
  c_real[offset + 1] = sums[1];
  c_real[offset + 2] = sums[2];
  c_real[offset + 3] = sums[3];
  c_imag[offset + 0] = sums[4];
  c_imag[offset + 1] = sums[5];
  c_imag[offset + 2] = sums[6];
  c_imag[offset + 3] = sums[7];
}
