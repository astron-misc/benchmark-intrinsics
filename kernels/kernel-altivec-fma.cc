#include <altivec.h>

#include "common.h"

#define VECTOR_LENGTH 4

inline vector float fmadd(vector float a, vector float b, vector float c) {
  __asm__ volatile("xvmaddmsp %0,%1,%2" : "+vf"(b) : "vf"(c), "vf"(a));
  return b;
}

inline vector float fmsub(vector float a, vector float b, vector float c) {
  __asm__ volatile("xvnmsubmsp %0,%1,%2" : "+vf"(b) : "vf"(c), "vf"(a));
  return b;
}

inline float vec_sum(vector float a) {
  float sum = 0.0f;
  for (unsigned short i = 0; i < VECTOR_LENGTH; i++) {
    sum += a[i];
  }
  return sum;
}

void kernel(const unsigned long long n, const float *a_xx_real,
            const float *a_xy_real, const float *a_yx_real,
            const float *a_yy_real, const float *a_xx_imag,
            const float *a_xy_imag, const float *a_yx_imag,
            const float *a_yy_imag, const float *b_real, const float *b_imag,
            float *__restrict__ c_real, float *__restrict__ c_imag) {
#if USE_ALIGNED
  ASSUME_ALIGNED
#endif

  float sums[8] = {0.0f};

  for (unsigned int r = 0; r < R; r++) {
    vector float c_xx_r = vec_splats(0.0f);
    vector float c_xy_r = vec_splats(0.0f);
    vector float c_yx_r = vec_splats(0.0f);
    vector float c_yy_r = vec_splats(0.0f);
    vector float c_xx_i = vec_splats(0.0f);
    vector float c_xy_i = vec_splats(0.0f);
    vector float c_yx_i = vec_splats(0.0f);
    vector float c_yy_i = vec_splats(0.0f);

    for (unsigned int i = 0; i < n; i += VECTOR_LENGTH) {
      vector float a_xx, a_xy, a_yx, a_yy;
      vector float b_r, b_i;

      b_r = vec_vsx_ld(0, &b_real[i]);
      b_i = vec_vsx_ld(0, &b_imag[i]);

      // Load real part of input
      a_xx = vec_vsx_ld(0, &a_xx_real[i]);
      a_xy = vec_vsx_ld(0, &a_xy_real[i]);
      a_yx = vec_vsx_ld(0, &a_yx_real[i]);
      a_yy = vec_vsx_ld(0, &a_yy_real[i]);

      // Update output
      c_xx_r = fmadd(c_xx_r, a_xx, b_r);
      c_xx_i = fmadd(c_xx_i, a_xx, b_i);
      c_xy_r = fmadd(c_xy_r, a_xy, b_r);
      c_xy_i = fmadd(c_xy_i, a_xy, b_i);
      c_yx_r = fmadd(c_yx_r, a_yx, b_r);
      c_yx_i = fmadd(c_yx_i, a_yx, b_i);
      c_yy_r = fmadd(c_yy_r, a_yy, b_r);
      c_yy_i = fmadd(c_yy_i, a_yy, b_i);

      // Load imag part of input
      a_xx = vec_vsx_ld(0, &a_xx_imag[i]);
      a_xy = vec_vsx_ld(0, &a_xy_imag[i]);
      a_yx = vec_vsx_ld(0, &a_yx_imag[i]);
      a_yy = vec_vsx_ld(0, &a_yy_imag[i]);

      // Update output
      c_xx_r = fmadd(c_xx_r, a_xx, b_i);
      c_xx_i = fmsub(c_xx_i, a_xx, b_r);
      c_xy_r = fmadd(c_xy_r, a_xy, b_i);
      c_xy_i = fmsub(c_xy_i, a_xy, b_r);
      c_yx_r = fmadd(c_yx_r, a_yx, b_i);
      c_yx_i = fmsub(c_yx_i, a_yx, b_r);
      c_yy_r = fmadd(c_yy_r, a_yy, b_i);
      c_yy_i = fmsub(c_yy_i, a_yy, b_r);
    }

    // Reduce all vectors
    sums[0] += vec_sum(c_xx_r);
    sums[1] += vec_sum(c_xy_r);
    sums[2] += vec_sum(c_yx_r);
    sums[3] += vec_sum(c_yy_r);
    sums[4] += vec_sum(c_xx_i);
    sums[5] += vec_sum(c_xy_i);
    sums[6] += vec_sum(c_yx_i);
    sums[7] += vec_sum(c_yy_i);
  }

  // Update output
  unsigned int offset = 4 * omp_get_thread_num();
  c_real[offset + 0] = sums[0];
  c_real[offset + 1] = sums[1];
  c_real[offset + 2] = sums[2];
  c_real[offset + 3] = sums[3];
  c_imag[offset + 0] = sums[4];
  c_imag[offset + 1] = sums[5];
  c_imag[offset + 2] = sums[6];
  c_imag[offset + 3] = sums[7];
}
