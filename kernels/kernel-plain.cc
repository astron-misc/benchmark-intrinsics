#include "common.h"

void kernel(const unsigned long long n, const float *a_xx_real,
            const float *a_xy_real, const float *a_yx_real,
            const float *a_yy_real, const float *a_xx_imag,
            const float *a_xy_imag, const float *a_yx_imag,
            const float *a_yy_imag, const float *b_real, const float *b_imag,
            float *__restrict__ c_real, float *__restrict__ c_imag) {
#if USE_ALIGNED
  ASSUME_ALIGNED
#endif

  float sums[8] = {0.0f};

  for (unsigned long r = 0; r < R; r++) {
    float c_xx_r = 0.0f;
    float c_xy_r = 0.0f;
    float c_yx_r = 0.0f;
    float c_yy_r = 0.0f;
    float c_xx_i = 0.0f;
    float c_xy_i = 0.0f;
    float c_yx_i = 0.0f;
    float c_yy_i = 0.0f;

#pragma omp simd reduction(+ : c_xx_r, c_xy_r, c_yx_r, c_yy_r, c_xx_i, c_xy_i, \
                               c_yx_i, c_yy_i)
    for (unsigned long i = 0; i < n; i++) {
      float a_xx, a_xy, a_yx, a_yy;
      float b_r, b_i;

      b_r = b_real[i];
      b_i = b_imag[i];

      // Load real part of input
      a_xx = a_xx_real[i];
      a_xy = a_xy_real[i];
      a_yx = a_yx_real[i];
      a_yy = a_yy_real[i];

      // Update output
      c_xx_r = c_xx_r + (a_xx * b_r);
      c_xx_i = c_xx_i + (a_xx * b_i);
      c_xy_r = c_xy_r + (a_xy * b_r);
      c_xy_i = c_xy_i + (a_xy * b_i);
      c_yx_r = c_yx_r + (a_yx * b_r);
      c_yx_i = c_yx_i + (a_yx * b_i);
      c_yy_r = c_yy_r + (a_yy * b_r);
      c_yy_i = c_yy_i + (a_yy * b_i);

      // Load imag part of input
      a_xx = a_xx_imag[i];
      a_xy = a_xy_imag[i];
      a_yx = a_yx_imag[i];
      a_yy = a_yy_imag[i];

      // Update output
      c_xx_r = c_xx_r - (a_xx * b_i);
      c_xx_i = c_xx_i + (a_xx * b_r);
      c_xy_r = c_xy_r - (a_xy * b_i);
      c_xy_i = c_xy_i + (a_xy * b_r);
      c_yx_r = c_yx_r - (a_yx * b_i);
      c_yx_i = c_yx_i + (a_yx * b_r);
      c_yy_r = c_yy_r - (a_yy * b_i);
      c_yy_i = c_yy_i + (a_yy * b_r);
    }

    // Update sums
    sums[0] += c_xx_r;
    sums[1] += c_xy_r;
    sums[2] += c_yx_r;
    sums[3] += c_yy_r;
    sums[4] += c_xx_i;
    sums[5] += c_xy_i;
    sums[6] += c_yx_i;
    sums[7] += c_yy_i;
  }

  // Update output
  unsigned int offset = 4 * omp_get_thread_num();
  c_real[offset + 0] = sums[0];
  c_real[offset + 1] = sums[1];
  c_real[offset + 2] = sums[2];
  c_real[offset + 3] = sums[3];
  c_imag[offset + 0] = sums[4];
  c_imag[offset + 1] = sums[5];
  c_imag[offset + 2] = sums[6];
  c_imag[offset + 3] = sums[7];
}
