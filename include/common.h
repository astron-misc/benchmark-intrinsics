#include <omp.h>

/*
        Number of benchmarks
*/
#define B 5

/*
        Number of loop iterations inside kernel
*/
#define R (unsigned long long)8192 * 32

/*
        Choose whether the kernel arguments
        should be considered aligned or not
*/
#define USE_ALIGNED 1

/*
        Alignment
*/
#define ALIGNMENT 64

#define ASSUME_ALIGNED                                                         \
  a_xx_real = (const float *)__builtin_assume_aligned(a_xx_real, ALIGNMENT);   \
  a_xy_real = (const float *)__builtin_assume_aligned(a_xy_real, ALIGNMENT);   \
  a_yx_real = (const float *)__builtin_assume_aligned(a_yx_real, ALIGNMENT);   \
  a_yy_real = (const float *)__builtin_assume_aligned(a_yy_real, ALIGNMENT);   \
  a_xx_imag = (const float *)__builtin_assume_aligned(a_xx_imag, ALIGNMENT);   \
  a_xy_imag = (const float *)__builtin_assume_aligned(a_xy_imag, ALIGNMENT);   \
  a_yx_imag = (const float *)__builtin_assume_aligned(a_yx_imag, ALIGNMENT);   \
  a_yy_imag = (const float *)__builtin_assume_aligned(a_yy_imag, ALIGNMENT);   \
  b_real = (const float *)__builtin_assume_aligned(b_real, ALIGNMENT);         \
  b_imag = (const float *)__builtin_assume_aligned(b_imag, ALIGNMENT);         \
  c_real = (float *)__builtin_assume_aligned(c_real, ALIGNMENT);               \
  c_imag = (float *)__builtin_assume_aligned(c_imag, ALIGNMENT);

/*
        Kernel
*/
void kernel(const unsigned long long n, const float *a_xx_real,
            const float *a_xy_real, const float *a_yx_real,
            const float *a_yy_real, const float *a_xx_imag,
            const float *a_xy_imag, const float *a_yx_imag,
            const float *a_yy_imag, const float *b_real, const float *b_imag,
            float *c_real, float *c_imag);
